import React from 'react';

import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome5';
import theme from '../theme/theme';


function BluetoothItem(props) {
    return (
        <TouchableOpacity style={styles.btItem}
                          onPress={props.onPress}>
            <Text>{props.device.name}</Text>
            {props.connected ?
                <Icon style={{marginLeft: 10}} name='check' size={20} color='green'/> :
                <Icon style={{marginLeft: 10}} name='times' size={20} color='red'/>}

        </TouchableOpacity>
    );
}

BluetoothItem.propTypes = {
    connected: PropTypes.bool,
    device: PropTypes.object.isRequired,
    onPress: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    btItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 12,
        marginVertical: 8,
        backgroundColor: theme.TERTIARY_COLOR,
    },
});

export default BluetoothItem;
