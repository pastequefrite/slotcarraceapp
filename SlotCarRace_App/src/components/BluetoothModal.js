import React from 'react';

import {SafeAreaView, View, Text, StyleSheet, Modal, FlatList, Alert} from 'react-native';
import PropTypes from 'prop-types';

import {connect, scan, isDeviceConnected, disconnect} from '../back/ble';

import theme from '../theme/theme';

import IconButton from './IconButton';
import BluetoothItem from './BluetoothItem';
import ControlUnit from '../back/ControlUnit';


class BluetoothModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scanning: false,
            devices: [],
            error: null,
        };
    }

    componentDidMount(): void {
        this._scan(1);
    }

    _scan = (timeout) => {
        this.setState({
            scanning: true,
        });
        scan(timeout).then(devices => {
            this.setState({
                devices: devices,
                scanning: false,
            });
        });
    };

    _toggleConnection = (device) => {
        // If device connected -> disconnect and inversely.
        if (device.connected) {
            disconnect(device).then(data => {
                this._checkConnected(data);
                this.props.setControlUnit(null);
                Alert.alert('Disconnected successfully', 'Your are now disconnected from ' + device.name);
            });
        } else {
            connect(device).then(() => {
                this._checkConnected(device);
                this.props.setControlUnit(new ControlUnit(device));
                Alert.alert('Connected successfully', 'Your are now connected to ' + device.name, [{
                    text: 'Let\'s race !',
                    onPress: () => {
                        this.props.setShow(false);
                    },
                }]);
            });
        }
    };

    _checkConnected = (device) => {
        let currentDevices = this.state.devices;

        const deviceIndex = currentDevices.findIndex(d => d.id === device.id);
        isDeviceConnected(currentDevices[deviceIndex]).then(data => {
            currentDevices[deviceIndex].connected = data;
            this.setState({devices: currentDevices});
        });
    };

    render() {
        return (
            <Modal animationType='slide' visible={this.props.show}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={styles.container}>
                        <IconButton style={{position: 'absolute', zIndex: 1}} name='times'
                                    onPress={() => this.props.setShow(false)}/>
                        <Text style={styles.title}>Devices list</Text>
                        <IconButton style={{position: 'absolute', zIndex: 1, right: 0}} name='redo'
                                    disabled={this.state.scanning} onPress={() => this._scan(2)}/>

                        <View style={{flex: 1}}>
                            {this.state.error ? <Text style={styles.error}>Error: {error}</Text> : null}
                            <FlatList
                                ListEmptyComponent={
                                    <View style={styles.empty}>
                                        <Text style={styles.title}>No ControlUnits found</Text>
                                    </View>}
                                refreshing={this.state.scanning}
                                onRefresh={() => this._scan(2)}
                                contentContainerStyle={{flexGrow: 1}}
                                data={this.state.devices}
                                renderItem={({item}) =>
                                    <BluetoothItem
                                        onPress={() => this._toggleConnection(item)}
                                        device={item}
                                        connected={item.connected}
                                    />}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    </View>
                </SafeAreaView>
            </Modal>

        );
    }
}

BluetoothModal.propTypes = {
    show: PropTypes.bool,
    setShow: PropTypes.func.isRequired,
    setControlUnit: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: theme.MARGIN_HORIZONTAL,
        flex: 1,
    },
    error: {
        color: 'red',
        textAlign: 'center',
    },
    title: {
        textAlign: 'center',
        fontSize: theme.FONT_SIZE_XLARGE,
        color: theme.SECONDARY_COLOR,
    },
    empty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default BluetoothModal;
