import React, {useState} from 'react';

import {Text, View} from 'react-native';
import PropTypes from 'prop-types';

import Slider from '@react-native-community/slider';


function TuningSlider(props) {
    const [value, setValue] = useState(0);

    const _onSlidingComplete = (value) => {
        value = Math.round(value);
        props.onSlidingComplete(8, value);
        // setValue(value);
    };

    const _onValueChange = (value) => {
        value = Math.round(value);
        setValue(value);
    };

    return (
        <View>
            <Text style={props.titleStyles}>{props.title}</Text>
            <Text style={[{position : 'absolute', right: 0}, props.titleStyles]}>{value}</Text>
            <Slider minimumValue={0} maximumValue={15} value={value} onValueChange={(value) => _onValueChange(value)} onSlidingComplete={(value) => _onSlidingComplete(value)}/>
        </View>
    );
}

TuningSlider.propTypes = {
    title: PropTypes.string.isRequired,
    titleStyles: PropTypes.object,
    onSlidingComplete: PropTypes.func,
};

export default TuningSlider
