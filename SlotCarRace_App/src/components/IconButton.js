import React from 'react';

import {TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome5';
import theme from '../theme/theme';


function IconButton(props) {
    return (
        <TouchableOpacity style={props.style} disabled={props.disabled} onPress={props.onPress}>
            <Icon name={props.name} size={30} color={ props.disabled ? theme.DISABLED_COLOR :theme.SECONDARY_COLOR}/>
        </TouchableOpacity>
    );
}

IconButton.propTypes = {
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.object,
};

export default IconButton
