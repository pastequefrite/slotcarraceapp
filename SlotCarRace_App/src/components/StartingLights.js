import React from 'react';

import {View, StyleSheet} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome5';
import theme from '../theme/theme';


function StartingLights() {
    return (
        <View style={styles.container}>
            <Icon style={styles.light} name='circle' size={25} color={theme.SECONDARY_COLOR} solid/>
            <Icon style={styles.light} name='circle' size={25} color={theme.SECONDARY_COLOR} solid/>
            <Icon style={styles.light} name='circle' size={25} color={theme.SECONDARY_COLOR} solid/>
            <Icon style={styles.light} name='circle' size={25} color={theme.SECONDARY_COLOR} solid/>
            <Icon style={styles.light} name='circle' size={25} color={theme.SECONDARY_COLOR} solid/>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    light: {
        marginHorizontal: 2,
    },
});

export default StartingLights;
