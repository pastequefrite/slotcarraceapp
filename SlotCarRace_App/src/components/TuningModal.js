import React from 'react';

import {SafeAreaView, View, Text, StyleSheet, Modal} from 'react-native';

import PropTypes from 'prop-types';

import theme from '../theme/theme';

import IconButton from './IconButton';
import {Button, Input} from 'react-native-elements';
import ColorPalette from 'react-native-color-palette';

import TuningSlider from './TuningSlider';

class TuningModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
        };
    }

    render() {
        return (
            <Modal animationType='slide' visible={this.props.show}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={styles.container}>
                        <IconButton style={{position: 'absolute', zIndex: 1}} name='times' onPress={() => this.props.setShow(false)}/>
                        <Text style={styles.title}>Tune Player 1</Text>
                        <View style={{flex: 1}}>
                            {this.state.error ? <Text style={styles.error}>Error: {error}</Text> : null}

                            <Text style={styles.subTitle}>Name : </Text>
                            <Input placeholder={'Name :'} placeholderTextColor={theme.TERTIARY_COLOR}/>

                            <ColorPalette title={'Color :'} titleStyles={styles.subTitle}
                                          colors={['red', 'blue', 'yellow', 'black', 'green', 'orange']}/>

                            <TuningSlider title={'Speed :'} titleStyles={styles.subTitle} onSlidingComplete={this.props.cu.setSpeed}/>
                            <TuningSlider title={'Brakes :'} titleStyles={styles.subTitle} onSlidingComplete={this.props.cu.setSpeed}/>
                            <TuningSlider title={'Fuel :'} titleStyles={styles.subTitle} onSlidingComplete={this.props.cu.setSpeed}/>

                        </View>
                        <Button title='Apply'/>
                    </View>
                </SafeAreaView>
            </Modal>

        );
    }
}

TuningModal.propTypes = {
    show: PropTypes.bool,
    setShow: PropTypes.func.isRequired,
    cu: PropTypes.object,
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: theme.MARGIN_HORIZONTAL,
        flex: 1,
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    error: {
        color: 'red',
        textAlign: 'center',
    },
    title: {
        textAlign: 'center',
        fontSize: theme.FONT_SIZE_XLARGE,
        color: theme.SECONDARY_COLOR,
    },
    subTitle: {
        fontSize: theme.FONT_SIZE_LARGE,
        color: theme.SECONDARY_COLOR,
    },
    empty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default TuningModal;
