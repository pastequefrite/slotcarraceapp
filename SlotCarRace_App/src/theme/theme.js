export default {
    PRIMARY_COLOR: '#c0392b',
    SECONDARY_COLOR: 'grey',
    TERTIARY_COLOR: 'lightgrey',
    DISABLED_COLOR: 'lightgrey',
    FONT_SIZE_LARGE: 25,
    FONT_SIZE_XLARGE: 30,
    FONT_SIZE_MEDIUM: 20,
    FONT_SIZE_SMALL: 15,
    FONT_SIZE_XSMALL: 10,
    MARGIN_HORIZONTAL: 12,
};
