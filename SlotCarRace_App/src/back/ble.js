import {BleManager} from 'react-native-ble-plx';


const SERVICE_UUID = '39df7777-b1b4-b90b-57f1-7144ae4e4a6a';
const OUTPUT_UUID = '39df8888-b1b4-b90b-57f1-7144ae4e4a6a';
const NOTIFY_UUID = '39df9999-b1b4-b90b-57f1-7144ae4e4a6a';

const manager = new BleManager();


export const scan = (timeout, searchName) => {
    return new Promise(resolve => {
        const subscription = manager.onStateChange((state) => {
            if (state === 'PoweredOn') {
                let devices = [];
                manager.startDeviceScan(null, null, (error, device) => {
                    // Handle error (scanning will be stopped automatically)
                    if (error) {
                        manager.stopDeviceScan();
                        return error;
                    }

                    // If Device has got a name
                    if (device.name) {
                        // If Device not already in devices
                        if (devices.findIndex( d => d.id === device.id) === -1) {
                            // Check if device is connected
                            isDeviceConnected(device).then( data => {
                                device.connected = data;
                            });
                            // If searchName is Given, push it however, push all devices found
                            if (searchName) {
                                if (device.name === searchName) {
                                    devices.push(device);
                                }
                            } else {
                                devices.push(device);
                            }
                        }
                    }
                });
                subscription.remove();

                setTimeout(() => {
                    manager.stopDeviceScan();
                    resolve(devices);
                }, timeout * 1000);
            }
        }, true);
    });
};

export const isDeviceConnected = async (device) => {
    return await device.isConnected();
};

export const disconnect = async (device) => {
    return await device.cancelConnection();
};

export const connect = async (device) => {
    await device.connect()
        .then((device) => {
            // this.props.setShow(false);

            // Le seul service UUID
            // SERVICE_UUID = '39df7777-b1b4-b90b-57f1-7144ae4e4a6a'

            // Les seuls characteristic UUID
            // OUTPUT_UUID = '39df8888-b1b4-b90b-57f1-7144ae4e4a6a'
            // NOTIFY_UUID = '39df9999-b1b4-b90b-57f1-7144ae4e4a6a'

            return device.discoverAllServicesAndCharacteristics();
        })
        .then((device) => {
            device.monitorCharacteristicForService(SERVICE_UUID, NOTIFY_UUID, (error, characteristic) => {
                if (error) {
                    console.log(error.message);
                    return;
                }
                console.log(characteristic.value);
                console.log(atob(characteristic.value));
            });
            //string: 0
            //B64: MA==
            //HEX: 30
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'MA==').then(() => {
            //     console.log('Demande numéro de version');
            // });

            //string: ?
            //B64: Pw==
            //HEX: 3f
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'Pw==').then(() => {
            //     console.log('Demande temps tour');
            // });

            //string: ":T2$
            //B64: VDIk
            //HEX: 543224
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'VDIk').then(() => {
            //     console.log('Appuie sur le bouton start');
            // });

            //string: ":T1$
            //B64: VDEk
            //HEX: 543124
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'VDEk').then(() => {
            //     console.log('Appuie sur le Pace car ');
            // });

            //string: ":T5$
            //B64: VDUk
            //HEX: 543524
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'VDEk').then(() => {
            //     console.log('Appuie sur le Speed ');
            // });

            //string ":=101$
            //B64: Ijo9MTAxJA==
            //HEX: 223a3d31303124
            // device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, 'Ijo9MTAxJA==').then(() => {
            //     console.log('Maj début de course');
            // });

        })
        .then(() => {
            console.log('Listening...');
        }, (error) => {
            console.log(error.message);
        });
};

export const write = (device, message) => {
    //string: 0
    //B64: MA==
    //HEX: 30
    device.writeCharacteristicWithoutResponseForService(SERVICE_UUID, OUTPUT_UUID, message).then(() => {
        console.log('write');
    });
};

