import {write} from './ble';

const buttons = {
    ESC: 1,
    PACE_CAR: 1,
    ENTER: 2,
    START: 2,
    SPEED: 5,
    BRAKE: 6,
    FUEL: 7,
    CODE: 8,
};

const cmds = {
    J: 'J',
    T: 'T',
    SPEED: 0,
    BRAKES: 1,
    FUEL: 2,
};

const DOLLAR = '$';

const ControlUnit = class {
    constructor(device) {
        this.device = device;
    }

    // Press Start button
    toggleStart = () => {
        this.#pressButton(buttons.START);
    };

    // 1, 2, 3, 5, 6, 7, 9, 11, 13, 15
    // Set Speed level for a player
    setSpeed = (playerId, value) => {
        this.#set(0, playerId, value, 2);
    };

    // Set Brakes level for a player
    setBrakes = (playerId, value) => {
        this.#set(1, playerId, value, 2);
    };

    // Set Fuel level for a player
    setFuel = (playerId, value) => {
        this.#set(2, playerId, value, 2);
    };


    // PRIVATE FUNCTIONS

    #set = (cmd, id, value, repeat = 1) => {
        // encode to B64
        //'J00152$'
        // const msg = btoa(cmds.J + cmd + id + value + repeat + DOLLAR);
        const rawString = cmds.J + 0 + 0 + 15 + 2 + DOLLAR;
        console.log(rawString);
        const msg = btoa(rawString);
        console.log(msg);

        write(this.device, msg);
    };

    #pressButton = (button) => {
        const msg = btoa(cmds.T + button + DOLLAR);
        write(this.device, msg);
    }
};


export default ControlUnit;
