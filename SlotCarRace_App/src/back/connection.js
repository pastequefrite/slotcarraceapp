// use with T[num]$
ESC = 1,
PACE_CAR = 1,
ENTER = 2,
START = 2,
SPEED = 5,
BRAKE = 6,
FUEL = 7,
CODE = 8;

// Racers / users / joueurs (6 au total)
const MODELS = [0, 1, 2, 3, 4, 5].map(id => ({
    id: id,
    speed: null,
    brake: null,
    fuel: null
}));

fromCU = {
    'speed': [0, 1, 2, 3, 5, 6, 7, 9, 11, 13, 15],
    'brake': [0, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    'fuel':  [0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
};

toCU = {
    'speed': [1, 1, 2, 3, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10],
    'brake': [1, 1, 1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    'fuel':  [1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
};
