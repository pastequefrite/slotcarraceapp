import React, {useState} from 'react';

import {SafeAreaView, View, Text, StyleSheet, Button} from 'react-native';

import theme from '../theme/theme';

import StartingLights from '../components/StartingLights';
import StartButton from '../components/StartButton';
import BluetoothModal from '../components/BluetoothModal';
import TuningModal from '../components/TuningModal';

import PilotItem from '../components/PilotItem';
import IconButton from '../components/IconButton';

import {write, isDeviceConnected, connect} from '../back/ble';

function HomeScreen() {

    const [showBT, setShowBT] = useState(false);
    const [showTuning, setShowTuning] = useState(false);

    const [controlUnit, setControlUnit] = useState({});

    // const _bite = () => {
    //     isDeviceConnected(controlUnit).then(data => {
    //         console.log(data);
    //     });
    // };
    //
    // const _chatte = () => {
    //     connect(controlUnit).then(data => {
    //         console.log(data);
    //     })
    // };

    const _setDevice = () => {
        controlUnit.setSpeed(0, 5);
    };

    // const _test = () => {
    //     write(controlUnit, 'MA==');
    // };
    // const _start = () => {
    //     write(controlUnit, 'VDIk');
    // };
    // const _test2 = () => {
    //     write(controlUnit, 'Pw==');
    // };
    // const _paceCar = () => {
    //     write(controlUnit, 'VDEk');
    // };
    // const _speed = () => {
    //     write(controlUnit, 'VDUk');
    // };
    // const _testicule= () => {
    //     set(0, 0, 5, 2);
    // };

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <StartButton/>
                    <StartingLights/>
                    <IconButton name='bluetooth' onPress={() => setShowBT()}/>
                </View>
                <View>
                    <Text>{controlUnit.name}</Text>
                    <Button title={'Is Device Connected'} onPress={() => _bite()}/>
                    {/*<Button title={'Reconnect device'} onPress={() => _chatte()}/>*/}
                    {/*<Button title={'Get Version'} onPress={() => _test()}/>*/}
                    {/*<Button title={'Start'} onPress={() => _start()}/>*/}
                    {/*<Button title={'Pace car'} onPress={() => _paceCar()}/>*/}
                    {/*<Button title={'Speed'} onPress={() => _speed()}/>*/}
                    {/*<Button title={'Temps du dernier tour'} onPress={() => _test2()}/>*/}
                    {/*<Button title={'TEST Y CULE'} onPress={() => _testicule()}/>*/}
                    <Button title={'Configurer joueur 1'} onPress={() => setShowTuning(true)}/>

                    <Button title={'Set device'} onPress={() => _setDevice()}/>


                    <PilotItem pilot={{place: '1', name: 'Grégoire', bestLap: '2.998', lastLap: '3.046', laps: '6'}}/>
                    <PilotItem pilot={{place: '2', name: 'Justine', bestLap: '3.248', lastLap: '4.002', laps: '5', gap: '+1.034'}}/>
                </View>
            </View>

            <BluetoothModal setShow={setShowBT} show={showBT} setControlUnit={setControlUnit}/>
            <TuningModal setShow={setShowTuning} show={showTuning} cu={controlUnit}/>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: theme.MARGIN_HORIZONTAL,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});

export default HomeScreen;
